package machines.serveurs.gestionDePorts;

import machines.serveurs.FTP.ModuleDeGestionFichiersFTP;
import machines.serveurs.FTP.ServeurFTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Classe implémentant les méthodes qui gérent une multitude de clients en parallèle grace aux threads
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ModuleDeGestionClientsGDP implements Runnable
{

    ServeurGDP serveurGDP;
    Socket socket;

    /**
     * Constructeur de la classe ModuleDeGestionClientsGDP
     * @param serveurGDP le serveur
     * @param socket le socket utilisé
     */
    public ModuleDeGestionClientsGDP(ServeurGDP serveurGDP, Socket socket)
    {
        super();
        this.socket = socket;
        this.serveurGDP = serveurGDP;
    }

    /**
     * Méthode effectuant le traitement de plusieurs requete GDP en meme temps grace aux threads
     */
    public void run()
    {
        try
        {
            BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String chaine = entreeSocket.readLine();

            ModuleDeGestionFichierGDP mGDP = new ModuleDeGestionFichierGDP(this.serveurGDP);
            if (!mGDP.estDansFichier(chaine)) mGDP.ajouterCoupleServeurPort(chaine);

            socket.close();
        }
        catch (Exception e) {e.printStackTrace();}
    }
}
