package machines.serveurs.gestionDePorts;

import machines.Machine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * ServeurGDP instancie un serveur de gestion des ports
 * Quand plusieurs serveurs coexistent sur la meme machine la seule manière de les différencier
 * est en analysant les ports associés à chacun, c'est le role de ce serveur
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ServeurGDP extends Machine
{
    
	
    /**
     * Constructeur de la classe ServeurGDP
     * @param nomHote le nom d'hote de la machine
     */
    public ServeurGDP(String nomHote)
    {
        super(nomHote,"./src/machines/serveurs/gestionDePorts/fichiers/"+nomHote);
    }

    /**
     * Méthode fournissant le service attendu par un serveur GDP
     * @param socketEcoute la socket d'écoute du serveur
     */
    public void serviceGDP(ServerSocket socketEcoute) throws IOException
    {
        while(true)
        {
            // On attend une connexion puis on l'accepte
            Socket socket = socketEcoute.accept();

            ModuleDeGestionClientsGDP moduleDeGestionClient=new ModuleDeGestionClientsGDP(this, socket);
            Thread thread = new Thread(moduleDeGestionClient);
            thread.start();
        }
    }

    public static void main(String[] args) throws Exception
    {
        ServeurGDP serveur = new ServeurGDP("serveurGDP");
        ServerSocket socketEcoute = new ServerSocket(9292);
        serveur.serviceGDP(socketEcoute);
        socketEcoute.close();

        /*
        ServeurGDP serveur = new ServeurGDP("serveurGDP");
        ServerSocket socketEcoute = new ServerSocket(9292);
        serveur.serviceGDP(socketEcoute);*/
    }

}
