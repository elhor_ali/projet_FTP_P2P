package machines.serveurs.gestionDePorts;

import java.io.*;

/**
 * Classe de gestion du fichier GDP contenant les couples Serveurs-Ports
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ModuleDeGestionFichierGDP
{
    ServeurGDP serveurGDP;
    String fichierGDP;

    /**
     * Constructeur de la classe ModuleDeGestionFichierGDP
     * @param serveurGDP le serveur
     */
    public ModuleDeGestionFichierGDP(ServeurGDP serveurGDP)
    {
        this.serveurGDP = serveurGDP;
        this.fichierGDP = "fichierGDP.txt";
    }

    /**
     * Méthode de verification de l'existence d'un couple Serveur-Port dans le fichierGDP
     * @param coupleServeurPort le couple Serveur-Port
     * @return true si il existe, false sinon
     */
    public boolean estDansFichier(String coupleServeurPort) throws IOException
    {
        try (FileReader fileInvc = new FileReader(this.serveurGDP.getCheminDossierRacine()+"/"+fichierGDP);
             BufferedReader curseur = new BufferedReader(fileInvc)) {
            String ligne = curseur.readLine();
            while (ligne != null)
            {
                if (ligne.contains(coupleServeurPort)) return true;
                ligne = curseur.readLine();
            }
            return false;
        }
    }

    /**
     * Méthode permettant d'ajouter un couple Serveur-Port au fichier GDP
     * @param coupleServeurPort le couple Serveur-Port
     */
    public void ajouterCoupleServeurPort(String coupleServeurPort) throws IOException {
        FileWriter fw = new FileWriter(this.serveurGDP.getCheminDossierRacine()+"/"+fichierGDP,true);
        fw.write(coupleServeurPort+"\n");
        fw.close();
    }

    /*
    public String recupererListeServeurs()
    {
        String
    }
    */
}
