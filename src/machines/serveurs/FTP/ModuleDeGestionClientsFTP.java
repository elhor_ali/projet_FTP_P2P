
package machines.serveurs.FTP;

import java.io.*;
import java.net.Socket;

/**
 * Classe gérant les différents clients en parallèle à l'aide de threads
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ModuleDeGestionClientsFTP implements Runnable
{

    ServeurFTP serveurFTP;
    Socket socket;

    /**
     * Constructeur de la classe ModuleDeGestionClientsFTP
     * @param serveurFTP le serveur FTP en question
     * @param socket La socket utilisée
     */
    public ModuleDeGestionClientsFTP(ServeurFTP serveurFTP, Socket socket)
    {
        super();
        this.socket = socket;
        this.serveurFTP = serveurFTP;
    }

    /**
     * Implémentation du traitement en boucle des clients concernés
     */
    public void run()
    {

        ModuleDeGestionFichiersFTP moduleDeGestionFichiers = new ModuleDeGestionFichiersFTP(this.serveurFTP);
        BufferedReader entreeSocket = null;
        try {

            entreeSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Construction d'un PrintStream pour envoyer du texte à travers la connexion socket
            PrintStream sortieSocket = new PrintStream(socket.getOutputStream());

            // envoyer les noms des fichiers au client
            String[] listeNomsFichiers = moduleDeGestionFichiers.recupererListeFichiers();
            sortieSocket.println(listeNomsFichiers.length);
            for (int i=0; i<listeNomsFichiers.length; i++) sortieSocket.println(listeNomsFichiers[i]);
            String chaine = "";
            while(chaine!=null)
            {

                // lecture d'une chaine envoyée à travers la connexion socket
                chaine = entreeSocket.readLine();

                // si elle est nulle c'est que le client a fermé la connexion
                if (chaine != null)
                {
                    if (moduleDeGestionFichiers.fichierExiste(chaine))
                    {
                        sortieSocket.println("ok");
                        moduleDeGestionFichiers.envoyerFichier(socket,chaine);
                    }
                    else sortieSocket.println("ko");
                }
            }
            System.out.println("fin");
            // on ferme nous aussi la connexion
            socket.close();
        }
        catch (Exception e) {e.printStackTrace();}
    }

}
