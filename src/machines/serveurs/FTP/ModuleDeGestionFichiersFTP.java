
package machines.serveurs.FTP;

import java.io.*;
import java.net.Socket;

/**
 * Classe contenant les différentes méthodes de traitement de fichiers nécessaire pour le bon fonctionnement du serveur FTP
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ModuleDeGestionFichiersFTP
{

    ServeurFTP serveurFTP;

    /**
     * Constructeur de la classe ModuleDeGestionFichiersFTP
     * @param serveurFTP le serveur FTP
     */
    public ModuleDeGestionFichiersFTP(ServeurFTP serveurFTP)
    {
        this.serveurFTP = serveurFTP;
    }

    /**
     * Méthode permettant de récupérer la liste des fichiers disponibles sur ce serveur
     * @return String contenant les différents noms de fichiers
     */
    public synchronized String[] recupererListeFichiers()
    {
        File dir = new File(serveurFTP.getCheminDossierRacine());
        return dir.list();
    }

    /**
     * Vérifie si un fichier existe
     * @param nomFichier le nom du fichier
     * @return true si il existe, false sinon
     */
    public synchronized boolean fichierExiste(String nomFichier)
    {
        File f = new File(serveurFTP.getCheminDossierRacine()+"/"+nomFichier);
        if(f.exists() && !f.isDirectory()) {return true;}
        return false;
    }

    /**
     * Implémente l'envoie d'un fichier via une socket donné
     * @param socket le socket utilisé lors de la communication
     * @param nomFichier le nom du fichier
     */
    public synchronized void envoyerFichier(Socket socket, String nomFichier) throws Exception
    {
        DataOutputStream dataOutputStream = new DataOutputStream(
                new FilterOutputStream(socket.getOutputStream()) {
                    @Override
                    public void close() throws IOException {
                        //don't close socket!
                    }
                } );
        int bytes = 0;
        File file = new File(serveurFTP.getCheminDossierRacine()+"/"+nomFichier);
        FileInputStream fileInputStream = new FileInputStream(file);

        // send file size
        dataOutputStream.writeLong(file.length());
        // break file into chunks
        byte[] buffer = new byte[6*1024];
        while ((bytes=fileInputStream.read(buffer))!=-1)
        {
            dataOutputStream.write(buffer,0,bytes);
            dataOutputStream.flush();
        }
        fileInputStream.close();
        dataOutputStream.close();
    }
}
