

package machines.serveurs.FTP;

import machines.Machine;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Serveur FTP qui téléverse les différents fichiers aux clients
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class ServeurFTP extends Machine
{

    /**
     * Constructeur de la classe Serveur
     * @param nomHote le nom d'hote de la machine
     */
    public ServeurFTP(String nomHote)
    {
        super(nomHote,"./src/machines/serveurs/FTP/fichiers/"+nomHote);
    }

    /**
     * La méthode qui gére le service fournit par le serveur (boucle infinie plus traitement clients en parallèle)
     * @param socketEcoute le nom d'hote de la machine,
     */
    public void serviceFTP(ServerSocket socketEcoute) throws IOException
    {
        while(true)
        {
            // On attend une connexion puis on l'accepte
            Socket socket = socketEcoute.accept();

            ModuleDeGestionClientsFTP moduleDeGestionClient=new ModuleDeGestionClientsFTP(this, socket);
            Thread thread = new Thread(moduleDeGestionClient);
            thread.start();
        }
    }

    public static void main(String[] args) throws Exception
    {
        ServeurFTP serveur = new ServeurFTP("serveur_1");
        ServerSocket socketEcoute = new ServerSocket(9999);

        Socket socket = new Socket("127.0.0.1", 9292);
        PrintStream sortieSocket = new PrintStream(socket.getOutputStream());
        sortieSocket.println(serveur.getNomHote()+" "+"9998");
        socket.close();


        serveur.serviceFTP(socketEcoute);
    }













}
