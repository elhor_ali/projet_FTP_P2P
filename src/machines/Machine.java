

package machines;

/**
 * Classe abstraite qui définie une machine dans notre système
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public abstract class Machine
{
    private String nomHote;
    private String cheminDossierRacine;

    /**
     * Constructeur de la classe Machine
     * @param nomHote le nom d'hote de la machine
     * @param cheminDossierRacine le chemin vers le dossier racine de la machine
     * @return void
     */
    public Machine(String nomHote, String cheminDossierRacine)
    {
        this.nomHote = nomHote;
        this.cheminDossierRacine = cheminDossierRacine;
    }

    /**
     * Constructeur de la classe Machine
     * @return le chemin de la racine
     */
    public String getCheminDossierRacine() {
        return cheminDossierRacine;
    }

    /**
     * Constructeur de la classe Machine
     * @return Le nom d'hote
     */
    public String getNomHote() {
        return nomHote;
    }
}
