

package machines.clients;


import machines.Machine;

import java.io.*;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

/**
 * Client implémente un client pouvant télécharger du contenu disponible sur un serveur FTP
 * @author GOMEZ Sandra, ELHOR Ali, BENAMROUCHE Loukmane
 * @version 1.0
 * @since 12-2021
 * @link www.stri.fr
 **/
public class Client extends Machine
{

    /**
     * Constructeur de la classe Client
     * @param nomHote le nom d'hote de la machine
     */
    public Client(String nomHote)
    {
        super(nomHote,"./src/machines/clients/fichiers/"+nomHote);
    }

    /**
     * Méthode de téléchargement de fichier
     * @param socket le socket utilisé
     * @param emplacementFichier l'emplacement dans lequel le fichier sera stocké
     */
    private void telechargerFichier(Socket socket, String emplacementFichier) throws Exception
    {
        DataInputStream dataInputStream = new DataInputStream(
                new FilterInputStream(socket.getInputStream())
                {
                    @Override
                    public void close() throws IOException {/*don't close socket!*/}
                });

        int bytes;

        FileOutputStream fileOutputStream = new FileOutputStream(emplacementFichier);

        long size = dataInputStream.readLong();     // read file size
        byte[] buffer = new byte[4*1024];
        while (size > 0 && (bytes = dataInputStream.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1) {
            fileOutputStream.write(buffer,0,bytes);
            size -= bytes;      // read upto file size
        }
        fileOutputStream.close();
        dataInputStream.close();
    }

    /**
     * Méthode gérant les actions d'un client
     * @param socket le socket utilisé
     */
    public void gestionActions(Socket socket) throws Exception
    {
        // Construction d'un BufferedReader pour lire du texte envoyé à travers la connexion socket
        BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        // Construction d'un PrintStream pour envoyer du texte à travers la connexion socket
        PrintStream sortieSocket = new PrintStream(socket.getOutputStream());

        String chaine;
        String temp;
        int nbFichiers;
        // Scanner sur System.in
        Scanner scanner = new Scanner(System.in);


        //afficher le nombre de fichiers dans un serveur
        //chaine content le nb de fichiers
        chaine = entreeSocket.readLine();
        System.out.println("Le nombre de fichiers dans le serveur est " +chaine);
        //affichage des noms des fichiers
        nbFichiers= Integer.parseInt(chaine) ;

        for (int i=0; i<nbFichiers; i++)
        {
            chaine = entreeSocket.readLine();
            System.out.println(chaine);
        }

        while (true)
        {
            System.out.println("Tapez vos phrases ou FIN pour arrêter :");
            // lecture clavier
            chaine = scanner.nextLine();
            // si le client tappe FIN on ferme la session
            if (chaine.equalsIgnoreCase("FIN")) break;
            else {
                temp = chaine;
                sortieSocket.println(chaine); // on envoie la chaine au serveur

                // lecture d'une chaine envoyée à travers la connexion socket
                chaine = entreeSocket.readLine();
                if (Objects.equals(chaine, "ok")) {
                    this.telechargerFichier(socket, this.getCheminDossierRacine() + "/" + temp);
                }
                System.out.println("Chaine reçue : " + chaine);
            }
        }
    }

    public static void main(String[] args) throws Exception
    {
        Client client = new Client("client_1");

        Socket socket = new Socket("127.0.0.1", 9999);
        client.gestionActions(socket);
        socket.close();
    }

}
